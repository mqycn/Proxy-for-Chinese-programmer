# Sublime Text Package Control 安装

#### 路径说明

将本源码下载到本地，安装到您的任意站点任意目录（需要服务器支持 IPv6），安装地址为：
> http://您的域名/安装路径/proxy.php/https://packagecontrol.io/installation

也可以使用我已经部署好的地址：
> http://hk.miaoqiyuan.cn/products/proxy.php/https://packagecontrol.io/installation



#### 错误原因

```
Error installing Package Control: HTTPS error encountered, falling back to HTTP - <urlopen error [Errno 60] Operation timed out>
Error installing Package Control: HTTP error encountered, giving up - <urlopen error [Errno 60] Operation timed out>
error: An error occurred installing Package Control

Please check the Console for details

Visit https://packagecontrol.io/installation for manual instructions
```


#### 使用说明

 **1. 直接访问上面提到的安装地址，获取安装代码 ** 

程序会自动替换安装脚本的网络路径，已我已经部署好的地址为例，安装代码为:

![获取代码](https://images.gitee.com/uploads/images/2019/0211/225210_700449c1_82383.png "WX20190211-224849@2x.png")

```
import urllib.request,os,hashlib; h = '6f4c264a24d933ce70df5dedcf1dcaee' + 'ebe013ee18cced0ef93d5f746d80ef60'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://www.miaoqiyuan.cn/products/proxy.php/http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```

 **2. 打开Sublime Text 控制台，复制代码安装 ** 

打开 Sublime Text，在 View 菜单下，找到 Show Console。在底部控制台复制上面的代码后，按回车，很快就会安装完毕（和官方安装方法一致）。

![安装](https://images.gitee.com/uploads/images/2019/0211/225243_95aef115_82383.png "WX20190211-225030@2x.png")


 **3 安装完毕，更改 channels**

   请参考教程:[Sublime Text Package Control 无法使用](sublime-text-package-control.md)
