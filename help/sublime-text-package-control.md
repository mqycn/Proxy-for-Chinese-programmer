# Sublime Text Package Control 代理

#### 路径说明

将本源码下载到本地，安装到您的任意站点任意目录（需要服务器支持 IPv6），安装地址为：
> http://您的域名/安装路径/proxy.php/proxy/channel_v3.json

因为 proxy.php/https://packagecontrol.io/channel_v3.json 太大，会导致PHP输出不全，修改为静态模式。

使用之前 先 访问 http://您的域名/安装路径/proxy.php/https://packagecontrol.io/channel_v3.json，会自动重建 proxy/channel_v3.json

后期如果需要 更新缓存，也需要 再次执行 http://您的域名/安装路径/proxy.php/https://packagecontrol.io/channel_v3.json


也可以使用我已经部署好的地址：
> http://hk.miaoqiyuan.cn/products/proxy/channel_v3.json

如果文件过老，请先访问下面的地址更新下：
> http://hk.miaoqiyuan.cn/products/proxy.php/https://packagecontrol.io/channel_v3.json

#### 错误原因

```
Package Control: Attempting to use Urllib downloader due to WinINet error: Error downloading channel. Connection refused (errno 12029) during HTTP write phase of downloading https://packagecontrol.io/channel_v3.json.
Package Control: Error downloading channel. URL error [WinError 10060] 由于连接方在一段时间后没有正确答复或连接的主机没有反应，连接尝试失败。 downloading https://packagecontrol.io/channel_v3.json.
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/174640_c950871b_82383.png "0.png")


#### 使用说明

 **1. 打开 Settings User** 

打开 Sublime Text，选择 references -> Package Setting -> Package Control -> Settings User

![Windows Settings User](https://images.gitee.com/uploads/images/2019/0211/175128_2274689a_82383.png "Settings User.png")

![macOS Settings User](https://images.gitee.com/uploads/images/2019/0211/230120_6c02d70e_82383.png "WX20190211-230033@2x.png")

 **2. 输入配置信息** 
打开配置文件后，输入 channels 信息（请参考页面顶部**路径说明** ，以线上地址为例）：

```
"channels": [
	"http://hk.miaoqiyuan.cn/products/proxy.php/https://packagecontrol.io/channel_v3.json"
],
```

这里输入代码

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/175144_c9943492_82383.png "2.png")


 **3. 保存后，Package Control 已经可以正常使用了** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0211/175156_8b461e88_82383.png "4.png")