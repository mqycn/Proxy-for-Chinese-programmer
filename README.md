# Proxy for Chinese programmer

#### 从“痛苦”说起

最为一个中国的开发者，吃饭的家伙经常 因为 墙 的问题 没法使用。虽然 阿里、网易 的镜像能解决大部分问题，但是对于小众的软件就只没办法了（比如 神器 Sublime Text），每次出问题都把自己折腾的焦头烂额，影响工作进度。

 ** 折腾前请 查阅 页面底部 国内知名镜像站点，如果需要的项目存在镜像，没必要重复折腾 ** 

比如 Sublime Text，之前从同行的博客分享中找到了 channel_v3.json，他的站点因为备案问题，今天也不能访问了。。。

自己动手丰衣足食，今天抽空写了 一个 简单的代理脚本。既然要弄，就弄的完美一点：

 **1、支持自动更新** 
虽然之前可以使用，但是因为 是手工保存到服务器静态文件，只能使用老的插件。现在设置的每两小时更新一次

 **2、在官网出现故障时仍能访问** 
在自动和官网同步时，会 判断 官网是否返回正确的代码

 **3、一套最好能支持多个代理**
 借用 PHP 的PATH_INFO，可以非常方面的传入任何 URL，可以对全网实现代理。当然，本程序也提供了白名单。 

#### 代理的前提

1、对于被墙的代理，必须将域名放到 境外服务器

2、对于Sublime Text，服务器必须支持 IPv6

#### 白名单设置说明

配置文件在： proxy/conf/white.json

```
[
  {
    "pattern": "/https\\:\\/\\/packagecontrol\\.io\\/channel_v3\\.json/",
    "verify": "/\"repositories\"/"
  },
  {
    "pattern": "网址正则正则表达式，只有在列表中匹配到的url才可以访问",
    "verify": "内容正则正则表达式，只有返回的内容能匹配本规则，才会写入缓存。也可以为false，不判断直接写入缓存"
  },
]
```


#### 已经处理的问题

[Sublime Text Package Control 无法安装](help/sublime-text-package-control-install.md)

[Sublime Text Package Control 无法使用](help/sublime-text-package-control.md)


#### 国内知名镜像站点（更新中）

[腾讯软件源 http://mirrors.tencent.com/](http://mirrors.tencent.com/)

[阿里巴巴开源镜像站 https://opsx.alibaba.com/mirror](https://opsx.alibaba.com/mirror)

[华为开源镜像站 https://mirrors.huaweicloud.com/](https://mirrors.huaweicloud.com/)

[网易开源镜像站 http://mirrors.163.com/](http://mirrors.163.com/)

[搜狐开源镜像站 http://mirrors.sohu.com/](http://mirrors.sohu.com/)

#### 国内高校镜像站（更新中）

[清华大学开源软件镜像站 https://mirrors.tuna.tsinghua.edu.cn/](https://mirrors.tuna.tsinghua.edu.cn/)

[北京理工大学开源软件镜像站 http://mirror.bit.edu.cn/web/](http://mirror.bit.edu.cn/web/)

[华中科技大学开源镜像站 http://mirror.hust.edu.cn/](http://mirror.hust.edu.cn/)

[上海交通大学开源镜像站 http://ftp.sjtu.edu.cn/html/resources.xml](http://ftp.sjtu.edu.cn/html/resources.xml)

[中国科学技术大学开源软件镜像 http://mirrors.ustc.edu.cn/](http://mirrors.ustc.edu.cn/)